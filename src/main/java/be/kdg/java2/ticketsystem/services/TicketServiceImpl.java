package be.kdg.java2.ticketsystem.services;

import be.kdg.java2.ticketsystem.domain.HardwareTicket;
import be.kdg.java2.ticketsystem.domain.Ticket;
import be.kdg.java2.ticketsystem.domain.TicketState;
import be.kdg.java2.ticketsystem.repository.DummyTicketRepository;
import be.kdg.java2.ticketsystem.repository.HardcodedTicketRepository;
import be.kdg.java2.ticketsystem.repository.TicketRepository;

import java.time.LocalDateTime;
import java.util.List;

public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;

    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    //In the service layer we do not use the CRUD naming
    @Override
    public Ticket addTicket(int accountId, String question) {
        Ticket ticket = new Ticket();
        ticket.setAccountId(accountId);
        ticket.setDateOpened(LocalDateTime.now());
        ticket.setState(TicketState.OPEN);
        ticket.setText(question);
        return ticketRepository.createTicket(ticket);
    }

    @Override
    public Ticket addTicket(int accountId, String devicename, String question) {
        HardwareTicket ticket = new HardwareTicket();
        ticket.setDeviceName(devicename);
        ticket.setAccountId(accountId);
        ticket.setDateOpened(LocalDateTime.now());
        ticket.setState(TicketState.OPEN);
        ticket.setText(question);
        return ticketRepository.createTicket(ticket);
    }

    @Override
    public List<Ticket> getAllTickets(){
        return ticketRepository.readTickets();
    }
}
