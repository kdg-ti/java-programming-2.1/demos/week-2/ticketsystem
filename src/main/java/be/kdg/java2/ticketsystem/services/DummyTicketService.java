package be.kdg.java2.ticketsystem.services;

import be.kdg.java2.ticketsystem.domain.Ticket;

import java.util.ArrayList;
import java.util.List;

public class DummyTicketService implements TicketService{
    @Override
    public Ticket addTicket(int accountId, String question) {
        System.out.println("Dummy addTicket method called");
        return new Ticket();
    }

    @Override
    public Ticket addTicket(int accountId, String devicename, String question) {
        System.out.println("Dummy addTicket method called");
        return new Ticket();
    }

    @Override
    public List<Ticket> getAllTickets() {
        System.out.println("Dummy getAllTickets method called");
        return new ArrayList<>();
    }
}
