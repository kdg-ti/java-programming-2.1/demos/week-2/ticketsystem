package be.kdg.java2.ticketsystem.services;

import be.kdg.java2.ticketsystem.domain.Ticket;

import java.util.List;

public interface TicketService {
    //In the service layer we do not use the CRUD naming
    Ticket addTicket(int accountId, String question);
    Ticket addTicket(int accountId, String devicename, String question);
    List<Ticket> getAllTickets();
}
