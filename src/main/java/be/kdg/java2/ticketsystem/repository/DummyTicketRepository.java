package be.kdg.java2.ticketsystem.repository;

import be.kdg.java2.ticketsystem.domain.Ticket;

import java.util.ArrayList;
import java.util.List;

public class DummyTicketRepository implements TicketRepository{
    @Override
    public List<Ticket> readTickets() {
        System.out.println("Dummy readTickets method called");
        return new ArrayList<>();
    }

    @Override
    public Ticket createTicket(Ticket ticket) {
        System.out.println("Dummy createTicket method called");
        return new Ticket();
    }
}
