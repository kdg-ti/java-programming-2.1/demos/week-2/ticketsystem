package be.kdg.java2.ticketsystem.repository;

import be.kdg.java2.ticketsystem.domain.Ticket;

import java.util.List;

public interface TicketRepository {
    //Use Create - Read - Update - Delete naming for the methods in
    //the repository (CRUD)
    List<Ticket> readTickets();
    Ticket createTicket(Ticket ticket);
}
