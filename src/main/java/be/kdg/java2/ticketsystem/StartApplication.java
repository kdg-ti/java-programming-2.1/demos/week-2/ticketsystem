package be.kdg.java2.ticketsystem;

import be.kdg.java2.ticketsystem.presentation.View;
import be.kdg.java2.ticketsystem.repository.HardcodedTicketRepository;
import be.kdg.java2.ticketsystem.repository.TicketRepository;
import be.kdg.java2.ticketsystem.services.TicketService;
import be.kdg.java2.ticketsystem.services.TicketServiceImpl;

public class StartApplication {
    public static void main(String[] args) {
        TicketRepository ticketRepository = new HardcodedTicketRepository();
        TicketService ticketService = new TicketServiceImpl(ticketRepository);
        View view = new View(ticketService);
        view.showMenu();
    }
}
