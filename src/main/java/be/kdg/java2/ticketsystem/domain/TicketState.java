package be.kdg.java2.ticketsystem.domain;

public enum TicketState {
    OPEN, ANSWERED, CLIENTANSWER, CLOSED
}
