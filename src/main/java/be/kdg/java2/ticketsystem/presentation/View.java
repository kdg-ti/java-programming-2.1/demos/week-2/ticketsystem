package be.kdg.java2.ticketsystem.presentation;

import be.kdg.java2.ticketsystem.domain.Ticket;
import be.kdg.java2.ticketsystem.services.TicketService;
import be.kdg.java2.ticketsystem.services.TicketServiceImpl;

import java.util.Scanner;

public class View {
    private TicketService ticketService;
    private Scanner scanner = new Scanner(System.in);
    private boolean quit = false;

    public View(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void showMenu(){
        while(!quit) {
            System.out.println("Ticket System");
            System.out.println("=============");
            System.out.println("0) exit");
            System.out.println("1) show all tickets");
            System.out.println("2) add a ticket");
            System.out.println("Make a choice:");
            int choice = scanner.nextInt();
            handleChoice(choice);
        }
    }

    private void handleChoice(int choice) {
        switch (choice) {
            case 0 -> quit = true;
            case 1 -> showAllTickets();
            case 2 -> addATicket();
        }
    }

    private void showAllTickets() {
        ticketService.getAllTickets().forEach(System.out::println);
    }

    private void addATicket() {
        System.out.println("Account id: ");
        int accountId = scanner.nextInt();
        scanner.nextLine();//to remove the <enter> from buffer...
        System.out.println("Question:");
        String question = scanner.nextLine();
        Ticket ticket = ticketService.addTicket(accountId, question);
        System.out.println(ticket + " added!");
    }
}
